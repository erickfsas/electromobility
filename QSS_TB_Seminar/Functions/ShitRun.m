% NED = 0 - Run FTP-75
% NED = 1 - Run NEDC
%clear all;
clc;
NED = 1;
plots = 1;

%set common params
clear grd
grd.Nu{1} = 101;    grd.Un{1}.hi = 1;       grd.Un{1}.lo = -1;

% Select the right cycle to run
if (NED ~= 0) 
    ShitCyc = 'Europe: NEDC';
    load NEDCmod
else
    ShitCyc ='USA:    FTP-75';
    load FTPmod
end

if (NED ~= 0)
    drivCyc = 'NEDCmod';
    ShitCyc = 'Europe: NEDC';
    t_tot   = 1220;
    x_fin   = 10.9326;
    % create grid
    grd.Nx{1} = 51;   grd.Xn{1}.hi = 1;       grd.Xn{1}.lo = 0.01;
    % set initial state and final constrains
    grd.X0{1} = 0.5;    grd.XN{1}.hi = 0.51;    grd.XN{1}.lo = 0.5;
    % model parameters
    par.fc_thd      = 4.4;   % Fuel cutoff threshold
    par.widl_repl   = 0;     % Allow idle speed replacement
    par.X_mx_Ibt    = 0.99;  % Maximum battery current allowed (as proportion of actual maximum battery)
    par.tq_adj      = 0.99;  % Torque adjustment factor
else
    drivCyc = 'FTPmod';
    ShitCyc ='USA:    FTP-75';
    t_tot   = 1877;
    x_fin   = 16.45;
    % create grid
    grd.Nx{1} = 5001;   grd.Xn{1}.hi = 1;       grd.Xn{1}.lo = 0.05;
    % set initial state and final constrains
    grd.X0{1} = 0.5;    grd.XN{1}.hi = 0.56;    grd.XN{1}.lo = 0.3;
    % model parameters
    par.fc_thd      = 3.2;   % Fuel cutoff threshold
    par.widl_repl   = 1;     % Allow idle speed replacement
    par.X_mx_Ibt    = 0.95;  % Maximum battery current allowed (as proportion of actual maximum battery)
    par.tq_adj      = 0.99;  % Torque adjustment factor
end

% load driving cycle
load(drivCyc);

% define problem
clear prb
prb.W{1} = V_z;     % speed
prb.W{2} = D_z;     % acceleration
prb.W{3} = G_z;     % gear
prb.W{4} = Tq_z;    % Gearbox tq
prb.W{5} = w_z;     % Gearbox ang. velocity
prb.W{6} = dw_z;    % Gearbox ang. acceleration
prb.Ts   = 1;
prb.N    = t_tot;

% set options
options = dpm();
options.MyInf           = 1000;
options.Verbose         = 'on';
options.BoundaryMethod  = 'Line';
options.Iter            = 10;
options.Tol             = 1e-8;
options.FixedGrid       = 0;

%execute DPM
[res dyn] = dpm(@hev,par,grd,prb,options);

%calculate fuel and efficiency
m_FuelIdeal = res.C{1}';                % total fuel mass used
fuel_L      = sum(m_FuelIdeal)/.843;    % fuel volumen
fuel_kml    = x_fin /fuel_L;            % fuel econ in km/L
fuel_L100km = 100 /fuel_kml;            % fuel econ in L/100km

%U_conditioning;
u_inp = res.uopt';
% Configure the QSS model to run the right cycle
open('qss_hybrid_electric_vehicle_dp.mdl');
set_param('qss_hybrid_electric_vehicle_dp/Driving Cycle','cyclenr',ShitCyc)
sim('qss_hybrid_electric_vehicle_dp');

if( T_sim(end) == t_tot)
    disp(['Ran: ' drivCyc]);
    disp(['DPM fuel economy: ' num2str(fuel_L100km, 3)]);
    disp(['QSS fuel economy: ' num2str(Fecon_sim(end), 3)]);
else
    disp(['Simulation stopped at: ' num2str(T_sim(end), 4)]);
end

if (plots ~= 0)
    plot((1:length(u_inp)),u_inp);
    hold on
    plot((1:(length(u_inp) + 1)),res.X{1,1});
    hold off
end