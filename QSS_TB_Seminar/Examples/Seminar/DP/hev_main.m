% select cycle
% select cycle
NED = 0;
if (NED ~= 0)
    drivCyc = 'NEDCmod';
    ShitCyc = 'Europe: NEDC';
    t_tot   = 1220;
    x_fin   = 10.9326;
else
    drivCyc = 'FTPmod';
    ShitCyc ='USA:    FTP-75';
    t_tot   = 1877;
    x_fin   = 16.45;    
end

clearvars -regexp .+testarr
log_testarr.Xnl = 1;
log_testarr.Xnh = 1;
log_testarr.Unl = 1;
log_testarr.Unh = 1;
log_testarr.XNH = 1;
log_testarr.XNL = 1;
log_testarr.fct = 1;
log_testarr.tqa = 1;
log_testarr.dpm = 3.4;
log_testarr.qss = 3.4;

i_Xnh = 1; i_Xnl = .01;
i_Unh = 1; i_Unl = -1;
i_XNH = .6;
%for i_Xnh = 0.5:0.05:1
    %for i_Xnl = 0:0.05:0.5
        %for i_Unh = 1:-0.1:0.5
            %for i_Unl = -1:.1:0
                for i_XNH = .56:02:.6
                 for i_XNL = .1:0.05:.5
                    for i_fct = 3.2:0.2:5
                        %for i_tqa = .95:.01:1
                            try 
                                % load driving cycle
                                load(drivCyc);
                                
                                

                                % create grid
                                clear grd 
                                grd.Nx{1} = 51;    grd.Xn{1}.hi = i_Xnh;    grd.Xn{1}.lo = i_Xnl;
                                grd.Nu{1} = 101;    grd.Un{1}.hi = i_Unh;       grd.Un{1}.lo = i_Unl;
                                % set initial state and final constrains
                                grd.X0{1} = 0.5;    grd.XN{1}.hi = i_XNH;    grd.XN{1}.lo = i_XNL;

                                % model parameters
                                par.fc_thd      = i_fct; % Fuel cutoff threshold
                                par.widl_repl   = 1;     % Allow idle speed replacement
                                par.X_mx_Ibt    = 0.95;  % Maximum battery current allowed (as proportion of actual maximum battery)
                                par.tq_adj      = i_tqa; % Torque adjustment factor

                                % define problem
                                clear prb
                                prb.W{1} = V_z;     % speed
                                prb.W{2} = D_z;     % acceleration
                                prb.W{3} = G_z;     % gear
                                %
                                prb.W{4} = Tq_z;    % Gearbox tq
                                prb.W{5} = w_z;     % Gearbox ang. velocity
                                prb.W{6} = dw_z;    % Gearbox ang. acceleration
                                %}
                                prb.Ts = 1;
                                prb.N  = t_tot;

                                % set options
                                options = dpm();
                                options.MyInf = 1000;
                                options.Verbose     = 'off';
                                options.Warnings    = 'off';
                                options.BoundaryMethod = 'Line'; % also possible: 'none' or 'LevelSet';
                                options.Iter = 5;
                                options.Tol = 1e-8;
                                options.FixedGrid = 0;

                                log_testarr.Xnl(end+1) = i_Xnl;
                                log_testarr.Xnh(end+1) = i_Xnh;
                                log_testarr.Unl(end+1) = i_Unl;
                                log_testarr.Unh(end+1) = i_Unh;
                                log_testarr.XNH(end+1) = i_XNH;
                                log_testarr.XNL(end+1) = i_XNL;
                                log_testarr.fct(end+1) = i_fct;
                                log_testarr.tqa(end+1) = i_tqa;
                                
                                %execute DPM
                                [res dyn] = dpm(@hev,par,grd,prb,options);

                                %U_conditioning;
                                u_inp = res.uopt';

                                %calculate fuel and efficiency
                                m_FuelIdeal = res.C{1}';                % total fuel mass used
                                fuel_L      = sum(m_FuelIdeal)/.843;    % fuel volumen
                                fuel_kml    = x_fin /fuel_L;            % fuel econ in km/L
                                fuel_L100km = 100 /fuel_kml;            % fuel econ in L/100km
                                % Configure the QSS model to run the right cycle
                                set_param('qss_hybrid_electric_vehicle_dp/Driving Cycle','cyclenr',ShitCyc)
                                sim('qss_hybrid_electric_vehicle_dp');

                                if( T_sim(end) == t_tot)
                                    log_testarr.dpm(end+1) = fuel_L100km;
                                    log_testarr.qss(end+1) = Fecon_sim(end);
                                    disp(['Ran: ' drivCyc]);
                                    disp(['Un high:' num2str(i_Unh,3) '   Un low:' num2str(i_Unl,3) '   Tq_adj:' num2str(i_tqa,3)...
                                        '   XN high:' num2str(i_XNH,3) '   XN low:' num2str(i_XNL,3) '   Fuel cutoff thd:' num2str(i_fct,3)]);

                                    disp(['DPM fuel economy: ' num2str(fuel_L100km, 4)]);
                                    disp(['QSS fuel economy: ' num2str(Fecon_sim(end), 4) newline newline]);
                                else
                                    disp(['Simulation stopped at: ' num2str(T_sim(end), 4)]);
                                end
                            catch
                                    log_testarr.dpm(end+1) = 1000;
                                    log_testarr.qss(end+1) = 1000;
                                    disp(['Ran: ' drivCyc]);
                                disp(['Un high:' num2str(i_Unh,3) '   Un low:' num2str(i_Unl,3) '   Tq_adj:' num2str(i_tqa,3)...
                                        '   XN high:' num2str(i_XNH,3) '   XN low:' num2str(i_XNL,3) '   Fuel cutoff thd:' num2str(i_fct,3)]);
                                 disp(['Infeasible' newline newline]);
                            end
                        end
                    end
                end
        %    end
       % end
    %end
%end

disp('shit done! Enjoy!')