function S         	= dpm_setallfield(S1,options)

try
    if strcmp(options,'nan')
        value = nan;
    elseif strcmp(options,'zero')
        value = 0;
    elseif strcmp(options,'inf')
        value = inf;
    end

    S = S1;
    names = fieldnames(S1);
    for i=1:length(names)
        if isstruct(S1.(names{i}))
            S.(names{i}) = dpm_setallfield(S1.(names{i}), options);
        elseif iscell(S1.(names{i}))
            for j=1:numel(S1.(names{i}))
                S.(names{i}){j} = value;
            end
        elseif isnumeric(S1.(names{i})) || islogical(S1.(names{i}))
            S.(names{i}) = value;
        else
            S.(names{i}) = S1.(names{i});
        end
    end
catch
    error('mergestruct: S1 and S2 have different structures.')
end
