function in         = dpm_findl(A,vec)
da = A(2)-A(1);
in = 1+floor((vec-A(1))./da);
in = max(in,1);
in = min(in,length(A));
