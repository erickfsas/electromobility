function clear_waitbars()
        
    % clear all waitbars
    set(0,'ShowHiddenHandles','on')
    handles = get(0,'Children');
    for i=1:length(handles)
        if strcmp(get(handles(i),'name'),'DPM:Waitbar')
            delete(handles(i))
        end
    end
    set(0,'ShowHiddenHandles','off')        