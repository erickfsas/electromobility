function out       	= dpm_get_empty_out(model,inp,par,grd,options)
%GET_EMPTY_OUT   Gets an empty result struct
%   OUT   = GET_EMPTY_OUT(MODEL,OPTIONS)
%   Gets an empty output struct from model.
%
%   See also dpm_forward_sim
%
%   Author(s): Olle L. Sundstr�m, 29.11.2006
%   Copyright 2006- Olle L. Sundstr�m
if ~exist('options')
    options = 'nan';
end

[X C I out] = feval(model,inp,par);
out.X = X;
out.C = C;
out.I = I;
out = dpm_setallfield(out,options);
