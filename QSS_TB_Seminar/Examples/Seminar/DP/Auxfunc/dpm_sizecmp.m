function c          = dpm_sizecmp(a,b)
sa = size(a);
sb = size(b);
c = numel(sa)==numel(sb) & numel(a) == numel(b) & sum(sa==sb);
