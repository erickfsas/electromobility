function varargout  = dpm(varargin)
%DPM Dynamic Programming using Matrix operations version 1.1.1
%
%   OPTIONS = DPM() returns the standard options structure.
%
%   DPM(FUN,NX,NU)  saves a random test model with the filename FUN.m
%   and NX number of states and NU number of inputs.
%
%   [[OUT] DYN] = DPM(FUN,PAR,GRD,PRB,OPTIONS) whith FUN is a function 
%   handle with the model function. The DPM-function requires that the 
%   state and input grids are discretized and limited. The function assumes 
%   equally spaced grids and the GRD structure needed is defined as:
%     GRD.X0{.}    = 'initial state' (only used in forward simulation)
%     GRD.XN{.}.hi = 'final state upper constraint'
%     GRD.XN{.}.lo = 'final state lower constraint'
%     GRD.Nx{.}    = 'number of elements in the state grid'
%     GRD.Xn{.}.hi = 'upper boundary of the state grid'
%     GRD.Xn{.}.lo = 'lower boundary of the state grid'
%     GRD.Nu{.}    = 'number of elements in the input grid'
%     GRD.Un{.}.hi = 'upper boundary of the input grid'
%     GRD.Un{.}.lo = 'lower boundary of the input grid'
%   PRB contains the problem parameters
%     PRB.Ts     = time step
%     PRB.N      = number of time steps in problem (defines the problem 
%                  length)
%     [PRB.N0]   = start time index (only used in forward simulation)
%     [PRB.W{i}] = (optional) vectors with length PRB.T containing
%     disturbances. 
%   PAR is a user defined variable that is sent to the model function.
%     Useful if the model need some parameters defined outside the DPM
%     function.
%   OPTIONS is the options structure containing
%     OPTIONS.Waitbar     = 'off';  (on/off to show or hide waitbars)
%     OPTIONS.Verbose     = 'on'    (on/off command window status
%                                   notification)
%     OPTIONS.Warnings    = 'off';  (on/off to show or hide warnings)
%     OPTIONS.SaveMap     = 'on';   (on/off to save cost-to-go map)
%     OPTIONS.MyInf       = 1e4;    (a big number for infeaible states 
%                                   where out.I=1)
%     OPTIONS.Minimize    = 1;      (one/zero if minimizing or maximizing)
%     OPTIONS.InputType   = 'cd'    (string with the same number of 
%                                    characters as number of inputs.
%                                    contains the character 'c' if input is 
%                                    continuous or 'd' if discrete.
%                                    Default is all continuous.)
%     OPTIONS.BoundaryMethod = 'none', 'Line' or 'LevelSet'
%      if BoundaryMethod = 'Line'
%       OPTIONS.FixedGrid = 0;      (zero/one use grid as defined in GRD or
%                                    adjust the according to boundary line)
%       OPTIONS.Iter      = 10;     (maximum number of iterations when 
%                                    inverting model)
%       OPTIONS.Tol       = 1e-8;   (minimum tolerance when inverting
%                                     model)
%      endif
%     OPTIONS.gN{1}                 Cost matrix at the final time 
%                                    (must be of size(OPTIONS.gN{1}) = 
%                                    [GRD.Nx{1} GRD.Nx{2} ... GRD.Nx{.}]).
%     
%
%   OUT = DPM(DYN,FUN,PAR,GRD,PRB,OPTIONS) only calculates the forward
%   simulation using DYN calculated earlier.
%
%   OUT = DPM(N0,DYN,FUN,PAR,GRD,PRB,OPTIONS) only calculates the forward
%   simulation using DYN calculated earlier but from index N0 
%   (in the range 1->prb.N).
%
%   Up to 5e6 grid points (prod([GRD.Nx{1} GRD.Nx{2} ... GRD.Nx{.} 
%          GRD.Nu{1} GRD.Nu{2} ... GRD.Nu{.}])) works well.
%
%
%   EXAMPLES_______________________________________________________________
%     DPM can be used to get standard options
%        options = dpm();
%     
%     DPM can be used to generate a test model
%        dpm('model_test',3,2);
%   
%   FULL DYNAMIC PROGRAMMING EXAMPLES______________________________________
%   To calculate the optimal control for a test model:
%     dpm('model_test',2,2);
%     grd.Nx{1}    = 11; grd.Xn{1}.lo = -50; grd.Xn{1}.hi = 100; 
%     grd.Nx{2}    = 11; grd.Xn{2}.lo = -50; grd.Xn{2}.hi = 100; 
%     grd.Nu{1}    = 11; grd.Un{1}.lo = -5;  grd.Un{1}.hi = 5; 
%     grd.Nu{2}    = 11; grd.Un{2}.lo = -5;  grd.Un{2}.hi = 5; 
%     
%     grd.X0{1}    = 0;
%     grd.X0{2}    = 0;
%     grd.XN{1}.lo = 0; grd.XN{1}.hi = 20;
%     grd.XN{2}.lo = 0; grd.XN{2}.hi = 20;
%
%     prb.Ts     = 1/5;
%     prb.N      = 200*1/prb.Ts + 1;
%     prb.N0     = 1;
%     prb.W{1}   = rand(1,prb.N);
%     options    = dpm();
%     [out dyn]  = dpm('model_test',[],grd,prb,options);
%
%
%   HINTS FOR DEBUGGING____________________________________________________
%   The model function is called twice before the actual DP algorithm is
%   used in order to determine the number of states, inputs, and outputs. 
%   When adding breakpoints to the model function, please be aware of this.
%
%
%   REFERENCES_____________________________________________________________
%   When using DPM please cite:
%   Sundstrom, O. and Guzzella, L., "A Generic Dynamic Programming Matlab
%   Function", In Proceedings of the 18th IEEE International Conference on 
%   Control Applications, pages 1625-1630, Saint Petersburg, Russia, 2009
%
%   LICENSE________________________________________________________________
%   This Source Code Form is subject to the terms of the Mozilla Public 
%   License, v. 2.0. If a copy of the MPL was not distributed with this 
%   file, You can obtain one at http://mozilla.org/MPL/2.0/.
%
%   COPYRIGHT______________________________________________________________
%   Copyright 2008- Institute for Dynamic Systems and Control, Department 
%   of Mechanical and Process Engineering, ETH Zurich
%   Author: Olle L. Sundstrom
%   
%   CHANGELOG______________________________________________________________
%   v.1.0.6 (2010) Useable for MATLAB 2007 -- CD
%   v.1.0.6 (15-Dec-2010): Added OPTIONS.VERBOSE 'on'|{'off'}. Prints
%	status of DPM in the command window. Output equivalent to Waitbar but
%	with significantly less computational effort---SE
%   v.1.0.6 (July 2011) removed use of roundn
%   v.1.1.0 (15-Dec-2011) Added Level-Set-Method as an option and new 
%   forward simulation scheme as an option -- PE
%   v.1.1.1 (11-Oc-2012) fixed error handling in line 2394 --PE
%   v.1.1.2 (19-Juni-2013) added licence agreement header -- PE
try
    
varargout = {};
if nargin == 3
    if ~exist([varargin{1} '.m'],'file')
        str{1}  = '';        
        str{end+1}  = ['function [X, C, I, signals] = ' varargin{1} '(inp,par)'];
        str{end+1}  = '\n%% inp.X{i} states';
        str{end+1}  = '%% inp.U{i} inputs';
        str{end+1}  = '%% inp.W{i} disturbances (as defined in dis-struct)';
        str{end+1}  = '%% inp.Ts   time step';
        str{end+1}  = '%% par      struct including user defined parameters';
%         str{end+1}  = '%% lim.X{i} = ["lower" ; "upper"] (limits of each state)';
        str{end+1}  = '\n%% state update (out.X{i} must be set within model function)';
%         str{end+1}  = 'func = inp.Ts.*(par.a.*(inp.X{1}-inp.X{1}.^2/par.b)-inp.U{1});';
        for i=1:varargin{2}
            x = round(rand*(varargin{2}-1))+1;
            u = round(rand*(varargin{3}-1))+1;
            str{end+1}  = ['X{' num2str(i) '} = ' num2str(rand) '.*(inp.X{' num2str(x) '} + inp.U{' num2str(u) '} + inp.W{1})./inp.Ts + inp.X{' num2str(i) '};'];
        end
        str{end+1}  = '\n%% cost (out.C{1} must be set within model function)';
        str{end+1}  = 'C{1} = -inp.Ts.*inp.U{1};';
        str{end+1}  = '\n%% Infeasibility (out.I [zero=feasible/one=infeasible] must be set within model function)';
        str{end+1}  = '\n%% for example if the state is outside the grid or infeasible combinations of inputs and states occur.';
        str{end+1}  = '\n%% The cost of these state-input combinations will be set to options.MyInf by the DPM function.';
        str{end+1}  = 'I = 0;';
%         for i=2:varargin{2}
%             str{end+1}  = ['I = bitor(out.I,bitor(out.X{' num2str(i) '}<lim.X{' num2str(i) '}.lo, out.X{' num2str(i) '}>lim.X{' num2str(i) '}.hi));'];
%         end
        str{end+1}  = '\n%% store signals (store any other signals in the out struct)';
        for i=1:varargin{3}
            str{end+1} = ['signals.U{' num2str(i) '} = inp.U{' num2str(i) '};'];
        end
        fid = fopen([varargin{1} '.m'], 'w');
        for i=1:length(str)
            fprintf(fid, [str{i} '\n']);
        end
        fclose(fid);  
        return
    else
        error('DPM:Internal','Filename exist in the current directory')
    end
elseif nargin == 4
    model   = varargin{1};
    par     = varargin{2};
    grd     = varargin{3};
    dis     = varargin{4};
    inp     = dpm_get_empty_inp(grd,dis,'zero');
    varargout{1} = dpm_get_empty_out(model,inp,par,grd,'zero');
    return;
elseif nargin == 2
    error('DPM:Internal','Grid generation is not supported anymore, grd = dpm(Xset,Uset);');
%     varargout{1} = dpm_compose_grid(varargin{1},varargin{2});
%     warning('DPM:General','Initial state X0, final state constraints XN, and state limits Xn are set to default values.')
%     return
elseif nargin == 0
    options.Waitbar     = 'off';
    options.Verbose     = 'on';
    options.Warnings    = 'on';
    options.SaveMap     = 'on';
    options.MyInf       = 1e4;
    options.Minimize    = 1;
    options.BoundaryMethod = 'none';
    %options.FixedGrid   = 1;
    %options.Iter        = 10;
    %options.Tol         = 1e-8;
    
    varargout{1} = options;
    return;
elseif nargin == 5
    RunForward  = nargout > 1;
    RunBackward = 1;
    model   = varargin{1};
    par     = varargin{2};
    grd     = varargin{3};
    dis     = varargin{4};
    options = varargin{5};
elseif nargin == 6
    RunForward  = 1;
    RunBackward = 0;
    dyn     = varargin{1};
    model   = varargin{2};
    par     = varargin{3};
    grd     = varargin{4};
    dis     = varargin{5};
    options = varargin{6};
elseif nargin == 7
    RunForward  = 1;
    RunBackward = 0;
    dyn     = varargin{2};
    model   = varargin{3};
    par     = varargin{4};
    grd     = varargin{5};
    dis     = varargin{6};
    options = varargin{7};
    t0      = varargin{1};
end

% Check all inputs
if nargin==4 || nargin==5 || nargin==6 || nargin==7
    % If disturbance vectors are not set
    if ~isfield(dis,'W')
        dis.W = {};
    end
    grd = input_check_grd(grd,dis.N);
end

if exist('options','var')
    if isfield(options,'Warnings') && strcmp(options.Warnings,'on')
        warning('on','DPM:Backward')
        warning('on','DPM:Forward')
        warning('on','DPM:General')
    end
    
    if ~isfield(options,'CalcLine')
        
        %Backward Compatibility:
        if isfield(options,'InfCost')
            warning('DPM:Internal','The option ''InfCost'' has been renamed to ''MyInf''. Consider adjusting your code.')
            options.MyInf = options.InfCost;
            options = rmfield(options,'InfCost');
        end
        if isfield(options,'BoundaryLineMethod')
            warning('DPM:Internal','The option ''BoundaryLineMethod'' has been renamed to ''BoundaryMethod''. Consider adjusting your code.')
            options.BoundaryMethod = options.BoundaryLineMethod;
            options = rmfield(options,'BoundaryLineMethod');
        end
        if isfield(options,'HideWaitbar')
            warning('DPM:Internal','The option ''HideWaitbar'' has been renamed to ''Waitbar''. Consider adjusting your code.')
            if options.HideWaitbar
                options.Waitbar = 'off';
            else
                options.Waitbar = 'on';
            end
            options = rmfield(options,'HideWaitbar');
        end
         if isfield(options,'UseLine')
            warning('DPM:Internal','The option ''UseLine'' has been renamed to ''BoundaryMethod''. Consider adjusting your code.')
            if options.UseLine
                options.BoundaryMethod = 'Line';
            else
                options.BoundaryMethod = 'none';
            end
            options = rmfield(options,'UseLine');
        end
        
        
        % Sanity Check of options structure:
        fnames = fieldnames(options);
        onames = {'Waitbar';'Verbose';'Warnings';'SaveMap';'MyInf';'Minimize';'BoundaryMethod';'Iter';'Tol';'FixedGrid';'gN';'InputType';'CalcLine';'UseUmap';'UseLine';'UseLevelSet'};
        for ii = 1:length(fnames)
            ok = 0;
            for jj=1:length(onames)
                ok = ok || strcmp(fnames{ii},onames{jj});
            end
            if ~ok
                warning('DPM:Internal',['Unknown option: ''',fnames{ii},'''!'])
            end
        end
        clear fnames onames ii jj ok
        
        options.CalcLine = 0;
        
        if ~isfield(options,'SaveMap');
            options.SaveMap = 0;
        else
            %Backwards compatibility
            if ~isnumeric(options.SaveMap) 
                switch options.SaveMap
                    case 'on'
                        options.SaveMap = 1;
                    case 'off'
                        options.SaveMap = 0;
                    otherwise
                        warning('DPM:Internal','Unable to interpret the option "SaveMap". Using SaveMap = ''on''!' )
                        options.SaveMap = 1;
                end
            end
        end
        
        %interpret user input regarding boundary method
        if ~isfield(options,'BoundaryMethod')
            options.BoundaryMethod = '';
        end
        
        switch options.BoundaryMethod
            case 'none'
                options.UseLine = 0;
                options.UseLevelSet = 0;
            case 'Line'
                options.UseLine = 1;
                options.UseLevelSet = 0;
                options.UseUmap = 1;
            case 'LevelSet'
                options.UseLine = 0;
                options.UseLevelSet = 1;
                options.UseUmap = 0;
            otherwise
                disp('Boundary Method not specified! Using BoundaryMethod = ''none''.')
                options.BoundaryMethod = 'none';
                options.UseLine = 0;
                options.UseLevelSet = 0;
        end
        
        if ~isfield(options,'UseUmap')
            options.UseUmap = 1;
        end
        
        % Boundary Line Method
        if options.UseLine
            if length(grd.Nx)>1
                warning('DPM:Internal','Boundary-Line method works only with one-dimensional systems. Consider using the Level-Set method.')
            end
            if ~isfield(options,'FixedGrid')
                warning('DPM:Internal','Grid adaptation unspecified. Using options.FixedGrid=1.')
                options.FixedGrid = 1;
            end
            if ~isfield(options,'Tol')
                warning('DPM:Internal','Model inversion requires the specification of a tolerance. Using options.Tol=10-8.')
                options.Tol = 1e-8;
            end
            if ~isfield(options,'Iter')
                warning('DPM:Internal','Model inversion requires the specification of a maximum number of iterations. Using options.Iter=10.')
                options.Iter = 10;
            end
            
        end
        
        % Level Set Method:
        if options.UseLevelSet
            if length(grd.Nx)==1
                disp('For one-dimensional systems, consider using the Boundary-Line method.')
            end
            if ~options.SaveMap
                warning('DPM:Internal','Level-Set method needs cost-to-go for forward simulation. Setting SaveMap = ''on''.')
                options.SaveMap = 1;
            end
            options.UseUmap = 0;
        end
        
    else
        
    end
    
end


% DYNAMIC PROGRAMMING _____________________________________________________
% Returns the optimal cost-to-go dyn.Jo with the
% size of T x length(X1)
% and the optimal input matrix dyn.Uo with the size
% size of T x length(X1)
if RunBackward
    for i=1:length(grd.Xn)
        for j=1:length(grd.Xn{i})
            if grd.Xn{i}.lo > grd.Xn{i}.hi
                if length(grd.Xn{i}) > 1
                    error('DPM:Internal',['Upper state boundary for state ' num2str(i) ' is at instance ' num2str(j) ' smaller than the lower state boundary.'])
                else
                    error('DPM:Internal',['Upper state boundary for state ' num2str(i) ' is smaller than the lower state boundary.'])
                end
            end
        end
    end
    if ~options.CalcLine
        %grdXN = linspace(grd.Xn{1}.lo(end),grd.Xn{1}.hi(end),grd.Nx{1}(end))';
        
        % remove if 2d boundary line
%         if options.FixedGrid && isempty(find(grdXN>=grd.XN{1}.lo & grdXN<=grd.XN{1}.hi,1))
%             error('DPM:Internal','Final state constraints are too thight. Try to change to FixedGrid=0 \n\t or widen the final constraints or increase state resolution.')
%         else
        if isfield(options,'FixedGrid') && ~options.FixedGrid && min(grd.XN{1}.hi-grd.XN{1}.lo)/eps<grd.Nx{1}(end)
            error('DPM:Internal','Final state constraints are too tight to include grd.Nx{1}(end) points.\n\t Widen the final constraints.')
        end    
    end
    
    dyn = dpm_backward(model,par,grd,dis,options);
end
% EVAULATE RESULT _________________________________________________________
% Uses the optimal input matrix dyn.Uo to simulate
% the optimal trajectory.
% Returns res with all output and inputs of the model
if RunForward
    try
        out = dpm_forward(dyn,model,par,grd,dis,options);
    catch
        clear_waitbars();
        err = lasterror;
        fprintf('DPM:Forward simulation error \n \t Make sure the problem is feasible.\n')
        inp  = dpm_get_empty_inp(grd,dis,'zero');
        out  = dpm_get_empty_out(model,inp,par,grd,'nan');        
%         varargout{length(varargout)+1} = err;
    end
    varargout{length(varargout)+1} = out;
end
varargout{length(varargout)+1} = dyn;
warning('off','DPM:Backward')
warning('off','DPM:General')
warning('off','DPM:Forward')

catch
    err = lasterror;
    notify_user_of_error(err);
    for i=1:nargout
        varargout{i} = [];
    end    
end
