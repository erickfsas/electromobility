function [X C I out] = hev(inp,par)

%function [X C I out] = hev(inp,par)
%HEV Computes the resulting state-of-charge based on current state-
%   of-charge, inputs and drive cycle demand.
%   
%   [X C I out] = HEV(INP,PAR)
%
%   INP   = input structure
%   PAR   = user defined parameters
%
%   X     = resulting state-of-charge
%   C     = cost matrix
%   I     = infeasible matrix
%   out   = user defined output signals

%{
%
% VEHICLE
%   wheel radius            = 0.288 m
%   static fric coefficient = 0.013
%   rolling friction        = 142 N
%   aerodynamic coefficient = 0.42 Ns^2/m^2
%   vehicle mass            = 1115 kg
%
% Wheel speed (rad/s)
wv  = inp.W{1} ./ 0.288;
% Wheel acceleration (rad/s^2)
dwv = inp.W{2} ./ 0.288;
% Wheel torque (Nm)
Tv = (142.2 + 0.42.*inp.W{1}.^2 + 1115.*1.05*inp.W{2}) .* 0.288;

% TRANSMISSION
%   gearbox efficiency = 0.98
% gear ratios
r_gear =  [10.76 6.32 4.15 2.89 2.3];
% Crankshaft speed (rad/s)
wg  = (inp.W{3}>0) .* r_gear(inp.W{3} + (inp.W{3}==0)) .* wv;
% Crankshaft acceleration (rad/s^2)
dwg = (inp.W{3}>0) .* r_gear(inp.W{3} + (inp.W{3}==0)) .* dwv;
% Crankshaft torque (Nm). Already takes into account whether torque is
% positive or negative
Tg  = (inp.W{3}>0) .* (Tv>0)  .* Tv ./ r_gear(inp.W{3} + (inp.W{3}==0)) ./ 0.98...
    + (inp.W{3}>0) .* (Tv<=0) .* Tv ./ r_gear(inp.W{3} + (inp.W{3}==0)) .* 0.98;
%}

% Use Simulink's w and Tq instead of DPM's
%
Tg = inp.W{4};
wg = inp.W{5};
dwg = inp.W{6};
%}

% Powertrain characterization
Tau_e   = 0.2;
Tau_m   = 0.1;
w_idl   = 105;
T_CE_co = par.fc_thd;
H_u     = 4.29367799114e7;
P_aux = 300;

% TORQUE SPLIT
% Engine drag torque (Nm)
Te0  = dwg * Tau_e;
% Electric motor drag torque (Nm)
Tm0  = dwg * Tau_m;
% Total required torque (Nm)
Ttot = Te0.*(inp.U{1}~=1) + Tm0 + Tg;

% Torque provided by engine
we = wg;
if (par.widl_repl ~=0); we = max(w_idl,wg); end
Te  = (wg>0) .* (Ttot>0)  .* (1-inp.U{1}).*Ttot;
Tb  = (wg>0) .* (Ttot<=0) .* (1-inp.U{1}).*Ttot;
% Torque provided by electric motor
Tm  = (wg>0) .*    inp.U{1} .*       Ttot;
inps = (Te>0).*(we<100) + (Ttot<0).*(Tm>0);

% ENGINE
%
%   diesel lower heating value = 42937000 J/kg
%

load V_CE_map_ADVISOR;

m_dot_fuel = par.tq_adj .* interp2(T_CE_col,w_CE_row, fc_fuel_map,...
min(max(Te,T_CE_col(1)),T_CE_col(end)),...
min(max(we,w_CE_row(1)),w_CE_row(end)).*ones(size(Tm)),'linear');
%Pe = ((Te>(T_CE_co+.01)) .* (wg > w_idl)).* (m_dot_fuel .* H_u +P_aux);
Pe = ((Te>(T_CE_co+.01)) .* (we > w_idl)).* (m_dot_fuel .* H_u +P_aux);

% maximum engine torque
Tmax = T_CE_max;
% Maximum engine torque
Te_max = interp1(w_CE_row,Tmax,min(max(wg,w_idl),w_CE_row(end)),'linear','extrap');
% Calculate infeasible
ine = (Te > Te_max);


% MOTOR (from ADVISOR PM25)
load EM_ADVISOR;

% motor maximum and minimum torques (indexed by speed list)
Tmmax   = T_EM_max;
Tmmin   = -T_EM_max;
% motor torque list
Tm_list = T_EM_col;

% Electric motor efficiency
e = (wg~=0) .* interp2(Tm_list,w_EM_row,eta_EM,Tm,wg.*ones(size(Tm))) + (wg==0);
% Summarize infeasible
inm = (isnan(e)) + (Tm<0)  .* (Tm < interp1(w_EM_row,Tmmin,wg,'linear','extrap')) +...
                   (Tm>=0) .* (Tm > interp1(w_EM_row,Tmmax,wg,'linear','extrap'));
e(isnan(e)) = 1;
% Calculate electric power consumption
Pm =  (Tm<0) .* wg.*Tm.*e + (Tm>=0) .* wg.*Tm./e;

% BATTERY
% Battery current limitations
%   battery capacity            = 10 Ah 
%   maximum discharging current = 300 A
%   maximum charging current    = 300 A
%   minimum time to charge/discharge battery = 2 min
%
I_0 = 10; % Ah
t_ch = 2; %min
Q_BT_IC_rel = 50;

% Inputs:
%   - Pm: Motor power
%   - inp.X{1}: previous SOC
%
% Outputs:
%   - Pb: battery power
%   - Ib: battery current
%   - X{1}: new SOC
%

% New battery model
init_BT;

% electric charge
Q = inp.X{1} * Q_BT_0;

if(Pm < 0) % charging
    q = Q / Q_BT_0;
    p = Pm / I_0;
    U = (c_BT_L3*q+c_BT_L1+sqrt((c_BT_L3*q+c_BT_L1)^2+4*(c_BT_L4*q+c_BT_L2)*(-p)))/2;
elseif (Pm > 0) % discharging
    q = Q / Q_BT_0;
    p = Pm / I_0;
    U = (c_BT_E3*q+c_BT_E1+sqrt((c_BT_E3*q+c_BT_E1)^2+4*(c_BT_E4*q+c_BT_E2)*(-p)))/2;
else % idle
    U = c_BT_L1 + (Q .* (c_BT_L3/Q_BT_0));
end

Ib = Pm ./ U;
Pb = Ib .* U;
%Ib = (I_tmp < I_BT_max) ;
X{1}  = - Ib / (I_0 * 3600) + inp.X{1};


% Old battery model
%{
% Battery efficiency
% columbic efficiency (1 when charging)
e = (Pm>0) + (Pm<=0) .* 1;
% Battery internal resistance
r = 0.003;

im = (Pm>0) .* 300 + (Pm<=0) .* 300;
% Battery voltage

%v = interp1(soc_list, V_oc, inp.X{1},'linear*','extrap');
v = 39 + 15.6 * inp.X{1};

% Battery current
Ib  =   e .* (v-sqrt(v.^2 - 4.*r.*Pm))./(2.*r);
% New battery state of charge
X{1}  = - Ib / (10 * 3600) + inp.X{1};
% Battery power consumption
Pb =   Ib .* v;
%}

% Update infeasible
%inb = (U.^2 < 4.*c_BT_L2.*Pm) + (abs(Ib)>0.999.*I_BT_max);
inb = (U.^2 < 4.*c_BT_L2.*Pm) + (abs(Ib)>par.X_mx_Ibt.*I_BT_max);

% Set new state of charge to real values
X{1} = (conj(X{1})+X{1})/2;
Pb   = (conj(Pb)+Pb)/2;
Ib   = (conj(Ib)+Ib)/2;

% COST
% Summarize infeasible matrix
I = (inps+inb+ine+inm~=0);
sinf = sum(I,'all') >0;
tinf = sum(I,'all') >= length(I);
% Calculate cost matrix (fuel mass flow)
C{1}  = 1.15 .*(Pe / (H_u));


% SIGNALS
%   store relevant signals in out
out.Te = Te;
out.Tm = Tm;
out.Tb = Tb;
out.wg = wg;
out.Ib = Ib;
out.Pb = Pb;
out.Tema = Te_max;
out.Pe = Pe;
%out.uopt = inp.U{1};

% REVISION HISTORY
% =========================================================================
% DATE      WHO                 WHAT
% -------------------------------------------------------------------------
% 





